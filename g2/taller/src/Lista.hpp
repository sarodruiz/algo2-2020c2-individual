#include "Lista.h"

Lista::Lista() : _longitud(0), _primero(nullptr), _ultimo(nullptr) {
}

Lista::Nodo::Nodo(const int &elem) : dato(elem), siguiente(nullptr), anterior(nullptr) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    _destruir();
}

Lista& Lista::operator=(const Lista& aCopiar) {
    _destruir();
    int i = 0;
    while (i < aCopiar.longitud()) {
        agregarAtras(aCopiar.iesimo(i));
        i++;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo = new Nodo(elem);
    if (_longitud == 0) {
        _ultimo = nodo;
    } else {
        nodo->siguiente = _primero;
        _primero->anterior = nodo;
    }
    _primero = nodo;
    _longitud++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nodo = new Nodo(elem);
    if (_longitud == 0) {
        _primero = nodo;
    } else {
        nodo->anterior = _ultimo;
        _ultimo->siguiente = nodo;
    }
    _ultimo = nodo;
    _longitud++;
}

void Lista::eliminar(Nat i) {
    Nodo* actual = _primero;
    int n = 0;
    while (actual != nullptr and n < i) {
        actual = actual->siguiente;
        n++;
    }
    if (n == i) {
        if (actual->siguiente != nullptr) {
            actual->siguiente->anterior = actual->anterior;
        } else {
            _ultimo = actual->anterior;
        }
        if (actual->anterior != nullptr) {
            actual->anterior->siguiente = actual->siguiente;
        } else {
            _primero = actual->siguiente;
        }
        delete actual;
        _longitud--;
    }
}

int Lista::longitud() const {
    return this->_longitud;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* temp = _primero;
    int n = 0;
    while (n < i) {
        temp = temp->siguiente;
        n++;
    }
    return temp->dato;
    //assert(false);
}

int& Lista::iesimo(Nat i) {
    Nodo* temp = _primero;
    int n = 0;
    while (n < i) {
        temp = temp->siguiente;
        n++;
    }
    return temp->dato;
    //assert(false);
}

void Lista::mostrar(ostream& o) {
    Nodo* temp = _primero;
    while (temp != nullptr) {
        o << "[" << temp->dato << "] ";
        temp = temp->siguiente;
    }
    o << endl;
}

void Lista::sacarPrimero() {
    Nodo* temp = _primero;
    _primero = _primero->siguiente;
    delete temp;
    _longitud--;
}

void Lista::_destruir() {
    while (_primero != nullptr) {
        sacarPrimero();
    }
}
