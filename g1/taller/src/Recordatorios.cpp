#include <iostream>
#include "list"

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    bool operator<(Fecha o);

  private:
    //Completar miembros internos
    int mes_;
    int dia_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::mes() {
    return this->mes_;
}

int Fecha::dia() {
    return this->dia_;
}

void Fecha::incrementar_dia() {
    if (dia_ < dias_en_mes(mes_)) {
        dia_++;
    } else {
        dia_ = 1;
        if (mes_ < 12) {
            mes_++;
        } else {
            mes_ = 1;
        }
    }
}

ostream& operator<<(ostream& os, Fecha fecha) {
    os << fecha.dia() << "/" << fecha.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    // Completar iguadad (ej 9)
    bool igual_mes = this->mes() == o.mes();
    return igual_dia and igual_mes;
};
#endif

bool Fecha::operator<(Fecha o) {
    return ( this->mes_ < o.mes() or ( this->mes_ == o.mes() and this->dia_ < o.dia() ) );
}

// Ejercicio 11, 12

// Clase Horario

class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario o);
    bool operator<(Horario o);
private:
    uint hora_;
    uint min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {}

uint Horario::hora() {
    return this->hora_;
}

uint Horario::min() {
    return this->min_;
}

bool Horario::operator==(Horario o) {
    bool misma_hora = this->hora() == o.hora();
    bool mismo_min = this->min() == o.min();
    return misma_hora and mismo_min;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator<(Horario o) {
    return this->hora_ * 60 + this->min_ < o.hora() * 60 + o.min();
}

// Ejercicio 13

// Clase Recordatorio

class Recordatorio {
public:
    Recordatorio(Fecha fecha, Horario h, string msj);
    Fecha fecha();
    Horario h();
    string msj();
    bool operator<(Recordatorio o);
private:
    Fecha fecha_;
    Horario h_;
    string msj_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario h, string msj) : fecha_(fecha), h_(h), msj_(msj) {}

Fecha Recordatorio::fecha() {
    return this->fecha_;
}

Horario Recordatorio::h() {
    return this->h_;
}

string Recordatorio::msj() {
    return this->msj_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.msj() << " @ " << r.fecha() << " " << r.h();
    return os;
}

bool Recordatorio::operator<(Recordatorio o) {
    return ( this->fecha_ < o.fecha()  or ( this->fecha_ == o.fecha() and this->h_ < o.h() ) );
}

// Ejercicio 14

// Clase Agenda

class Agenda {
public:
    Agenda(Fecha fecha_iniciada);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    list<Recordatorio> recordatorios_de_hoy_;
    Fecha hoy_;
};

Agenda::Agenda(Fecha fecha_iniciada) : hoy_(fecha_iniciada) {}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_de_hoy_.push_back(rec);
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    return this->recordatorios_de_hoy_;
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

Fecha Agenda::hoy() {
    return this->hoy_;
}

ostream& operator<<(ostream& os, Agenda a) {
    list<Recordatorio> recs = a.recordatorios_de_hoy();
    recs.sort();
    os << a.hoy() << endl;
    os << "=====" << endl;
    for (Recordatorio r : recs) {
        if (r.fecha() == a.hoy()) {
            os << r << endl;
        }
    }
    return os;
}