
template<class T>
Conjunto<T>::Nodo::Nodo(const T &v) : valor(v), izq(nullptr), der(nullptr), padre(nullptr) {}

template <class T>
Conjunto<T>::Conjunto() {
    _raiz = nullptr;
    _cardinal = 0;
}

template <class T>
Conjunto<T>::~Conjunto() {
    destruirDesde(_raiz);
}

template<class T>
void Conjunto<T>::destruirDesde(Nodo *n) {
    if (n != nullptr) {
        destruirDesde(n->izq);
        destruirDesde(n->der);
        delete n;
    }
}

template<class T>
typename Conjunto<T>::Nodo* Conjunto<T>::elem(const T &clave, Nodo* raiz) const {
    Nodo* actual = raiz;
    while (not(esHoja(*actual)) and actual->valor != clave) {
        if (actual->valor > clave and actual->izq != nullptr) {
            actual = actual->izq;
        } else if (actual->valor < clave and actual->der != nullptr) {
            actual = actual->der;
        }
    }
    return actual;
}

template<class T>
bool Conjunto<T>::esHoja(const Nodo* n) const {
    return n->izq == nullptr and n->der == nullptr;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo* temp = _raiz;
    while (temp != nullptr and temp->valor != clave) {
        if (temp->valor > clave) {
            temp = temp->izq;
        } else {
            temp = temp->der;
        }
    }
    return temp != nullptr;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (_raiz == nullptr) {
        _raiz = new Nodo(clave);
        _cardinal++;
    } else {
        Nodo* temp = _raiz;
        while (temp->valor != clave) {
            if (temp->valor > clave) {
                if (temp->izq == nullptr) {
                    Nodo* nuevo = new Nodo(clave);
                    temp->izq = nuevo;
                    nuevo->padre = temp;
                    _cardinal++;
                } else {
                    temp = temp->izq;
                }
            } else {
                if (temp->der == nullptr) {
                    Nodo* nuevo = new Nodo(clave);
                    temp->der = nuevo;
                    nuevo->padre = temp;
                    _cardinal++;
                } else {
                    temp = temp->der;
                }
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (_cardinal == 1 and _raiz->valor == clave) {
        delete _raiz;
        _raiz = nullptr;
        _cardinal--;
    } else {
        removerDesde(_raiz->padre, "", _raiz, clave);
    }
}

template<class T>
void Conjunto<T>::removerDesde(Nodo* padre, string dir, Nodo* nodo, const T& valor) {
    if (nodo->valor == valor) {
        if (nodo->izq == nullptr and nodo->der == nullptr) {
            delete nodo;
            _cardinal--;
            if (dir == "IZQ") {
                padre->izq = nullptr;
            } else {
                padre->der = nullptr;
            }
        } else {
            if (nodo->izq != nullptr) {
                nodo->valor = maximoDesde(nodo->izq);
                removerDesde(nodo, "IZQ", nodo->izq, nodo->valor);
            } else {
                nodo->valor = minimoDesde(nodo->der);
                removerDesde(nodo, "DER", nodo->der, nodo->valor);
            }
        }
    } else {
        if (nodo->valor > valor) {
            removerDesde(nodo, "IZQ", nodo->izq, valor);
        } else {
            removerDesde(nodo, "DER", nodo->der, valor);
        }
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* temp = _raiz;
    while (temp != nullptr and temp->valor != clave) {
        if (temp->valor > clave) {
            temp = temp->izq;
        } else {
            temp = temp->der;
        }
    }
    if (temp->der != nullptr) {
        return minimoDesde(temp->der);
    } else {
        Nodo* padre = temp->padre;
        while (temp != padre->izq) {
            temp = padre;
            padre = temp->padre;
        }
        return padre->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    return minimoDesde(_raiz);
}

template<class T>
const T& Conjunto<T>::minimoDesde(Nodo *n) const {
    if (n->izq == nullptr) {
        return n->valor;
    } else {
        return minimoDesde(n->izq);
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    return maximoDesde(_raiz);
}

template<class T>
const T& Conjunto<T>::maximoDesde(Nodo *n) const {
    if (n->der == nullptr) {
        return n->valor;
    } else {
        return maximoDesde(n->der);
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream& os) const {
    os << "{";
    mostrarDesde(os, _raiz);
    os << "}" << endl;
}

template<class T>
void Conjunto<T>::mostrarDesde(std::ostream & os, Nodo * n) const {
    if (n != nullptr) {
        mostrarDesde(n->izq);
        os << n->valor << ",";
        mostrarDesde(n->der);
    }
}