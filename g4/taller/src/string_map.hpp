template <typename T>
string_map<T>::string_map() : _raiz(nullptr), _size(0) {}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {
    *this = aCopiar;
} // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    destruir(_raiz);
    _raiz = copiar(d._raiz);
    _size = d._size;
    return *this;
}

template<typename T>
typename string_map<T>::Nodo* string_map<T>::copiar(Nodo *n) {
    Nodo* nuevo;
    if (n == nullptr) {
        nuevo = nullptr;
    } else {
        nuevo = new Nodo();
        nuevo->hijos = n->hijos;
        if (n->definicion != nullptr) {
            nuevo->definicion = new T(*(n->definicion));
        } else {
            n->definicion = nullptr;
        }
        for (int i = 0; i < n->siguientes.size(); ++i) {
            if (n->siguientes[i] != nullptr) {
                nuevo->siguientes[i] = copiar(n->siguientes[i]);
            } else {
                nuevo->siguientes[i] = nullptr;
            }
        }
    }
    return nuevo;
}

template <typename T>
string_map<T>::~string_map(){
    destruir(_raiz);
}

template <typename T>
void string_map<T>::destruir(Nodo *raiz) {
    if (_raiz != nullptr) {
        for (int i = 0; i < raiz->siguientes.size(); ++i) {
            if (raiz->siguientes[i] != nullptr)
                destruir(raiz->siguientes[i]);
        }
        delete raiz->definicion;
        delete raiz;
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    if (_raiz == nullptr) {
        _raiz = new Nodo();
    }
    Nodo* actual = _raiz;
    int i = 0;
    while (i < clave.size()) {
        if (actual->siguientes[int(clave[i])] == nullptr) {
            actual->siguientes[int(clave[i])] = new Nodo();
            actual->hijos++;
        }
        actual = actual->siguientes[int(clave[i])];
        i++;
    }
    if (actual->definicion == nullptr) {
        actual->definicion = new T();
    }
    return *(actual->definicion);
}

template<typename T>
void string_map<T>::insert(const pair<string, T> & kv) {
    if (_raiz == nullptr) {
        _raiz = new Nodo();
    }
    Nodo* actual = _raiz;
    int i = 0;
    while (i < kv.first.size()) {
        if (actual->siguientes[int(kv.first[i])] == nullptr) {
            actual->siguientes[int(kv.first[i])] = new Nodo();
            actual->hijos++;
        }
        actual = actual->siguientes[int(kv.first[i])];
        i++;
    }
    if (actual->definicion == nullptr) {
        _size++;
    } else {
        delete actual->definicion;
    }
    actual->definicion = new T(kv.second);
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    Nodo* actual = _raiz;
    int i = 0;
    while (i < clave.size() and actual != nullptr) {
        actual = actual->siguientes[int(clave[i])];
        i++;
    }
    return int(actual != nullptr and actual->definicion != nullptr);
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* actual = _raiz;
    int i = 0;
    while (i < clave.size()) {
        actual = actual->siguientes[int(clave[i])];
        i++;
    }
    return *(actual->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* actual = _raiz;
    int i = 0;
    while (i < clave.size()) {
        actual = actual->siguientes[int(clave[i])];
        i++;
    }
    return *(actual->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    Nodo* actual = _raiz;
    Nodo* ultimo = nullptr;
    int i = 0;
    int j = 0;
    if (_raiz->definicion != nullptr or _raiz->hijos > 1) {
        ultimo = _raiz;
    }
    while (i < clave.size()) {
        actual = actual->siguientes[int(clave[i])];
        i++;
        if (actual->definicion != nullptr or actual->hijos > 1) {
            ultimo = actual;
            j = i;
        }
    }
    delete actual->definicion;
    actual->definicion = nullptr;
    _size--;
    if (ultimo != nullptr) {
        ultimo = ultimo->siguientes[int(clave[j])];
        j++;
        while (j < clave.size()) {
            Nodo* aux = ultimo;
            ultimo = ultimo->siguientes[int(clave[j])];
            delete aux;
            j++;
        }
    }
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;
}